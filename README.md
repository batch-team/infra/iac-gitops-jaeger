# GitOps repositiry for Jaeger deployment

## Short summary 
In order to start evaluating the tracing capabilities on our infrastructure services we concluded that Jaeger deployed on Kubernetes is a possible option.

This Jaeger deployment consists of a collector component that is writing directly to storage, instead of using Kafka as a preliminary buffer. An existing ElastiSearch 7 instance for storage backend (without Cassandra) is being deployed with all the sensitive information stored in the `releases/test/values.yaml` file being encrypted using [sops](https://github.com/mozilla/sops). The necessary configmap file is stored in `charts/jaeger/templates/store-cm.yaml`.

Both the deployment and the configuration of Jaeger are being managed using *GitOps* and *Flux*.

## Development

1. Create your custom branch.
1. Update the test release to point to your branch.
1. Deploy your custom branch:

```
$ CUSTOM_BRANCH=<your-branch>
helm upgrade -i helm-operator fluxcd/helm-operator --namespace flux --values helm.yaml --set allowNamespace=test
helm upgrade -i flux fluxcd/flux --namespace flux --version 1.3.0 --set git.url=https://gitlab.cern.ch/batch-team/infra/iac-gitops-jaeger --set git.branch=$CUSTOM_BRANCH  --values flux-values.yaml
```
## Handling secrets

As mentioned before, for handling sensitive information stored in helm yaml files, we rely on **sops** and its OpenStack Barbican Plugin to manage the encrypted content.

In the deployment automation we have two components: Flux and HelmOperator, but only Flux has a built-in sops support, which means that it will be able to decrypt the values when deploying. Therefore make sure that the decrypted file is stored either in the namespace or release folder.

Install sops client:
```
wget https://gitlab.cern.ch/cloud/sops/-/jobs/8834328/artifacts/raw/sops?inline=false
```
You may have to rename it.

## Editing secrets

Before editing a secret, make sure that you are at the right OpenStack environment and that you have run:

```
export OS_TOKEN=$(openstack token issue -c id -f value)
```

This can interfere with other openstack commands, so remember to unset if you find problems.

**Very Important**: When you want to modify a file that contains a sops encryption, even if it's not for modifying the encrypted values, make sure that you modify it using `sops` command (works similar to `nano`), otherwise you won't be able to decrypt the contents of the file.
